from tkinter import *
import time
from tkinter import font

# Screen
vw = 1920
vh = 1080
bg = '#161e2e'
fg = '#85c9a5'
status_text = '#e5e5e5'
danger_text = '#f7928b'
active_fg = '#28573d'

# GUI setting
GUI = Tk()
GUI.geometry('1920x1080')
GUI.state('zoomed')
GUI.title('Fishing user >w<')
GUI.configure(background=bg)

# Exit program
GUI.bind('<Command-x>', lambda x: GUI.destroy())

# Set fullscreen
GUI.attributes('-fullscreen', True)
GUI.bind('<Command-f>', lambda event:GUI.attributes('-fullscreen', not GUI.attributes('-fullscreen')))

# Font config
font_pixel = 'dogica pixel'
font_default = 'Thonburi'
title = (font_default, 22)

# Canvas
canvas = Canvas(GUI, width=vw, height=vh, bd=0, relief='ridge', highlightthickness=0)
canvas.configure(background=bg)
canvas.place(x=0,y=0)

a = canvas.create_rectangle(0, 0, 100, 40, fill=bg, outline=fg ,width=2)
canvas.move(a, 1170, 10)

# Border left content
border_status = canvas.create_rectangle(0, 0, 250, 100, fill=bg, outline=fg, width=2)
canvas.move(border_status, 10, 10)

border_commands = canvas.create_rectangle(0, 0, 250, 200, fill=bg, outline=fg, width=2)
canvas.move(border_commands, 10, 140)

border_terminal = canvas.create_rectangle(0, 0, 750, 250, fill=bg, outline=fg, width=2)
canvas.move(border_terminal, 10, 400)

border_listuser = canvas.create_rectangle(0, 0, 370, 500, fill=bg, outline=fg, width=2)
canvas.move(border_listuser, 900, 70)

# border list user
border_listuser1 = canvas.create_rectangle(0, 0, 350, 100, fill=bg, outline=fg, width=2)
canvas.move(border_listuser1, 910, 90)

border_listuser2 = canvas.create_rectangle(0, 0, 350, 100, fill=bg, outline=fg, width=2)
canvas.move(border_listuser2, 910, 210)

border_listuser3 = canvas.create_rectangle(0, 0, 350, 100, fill=bg, outline=fg, width=2)
canvas.move(border_listuser3, 910, 330)

border_listuser4 = canvas.create_rectangle(0, 0, 350, 100, fill=bg, outline=fg, width=2)
canvas.move(border_listuser4, 910, 450)

border_listuser4_active = canvas.create_rectangle(0, 0, 350, 100, fill=active_fg, outline=fg, width=2)
canvas.move(border_listuser4_active, 910, 450)

# Progress bar
border_progressbar = canvas.create_rectangle(0, 0, 560, 350, fill=bg, outline=fg, width=2)
canvas.move(border_progressbar, 300, 20)

border_download = canvas.create_rectangle(0, 0, 480, 30, fill=bg, outline=fg, width=2)
canvas.move(border_download, 340, 100)

border_download1 = canvas.create_rectangle(0, 0, 360, 30, fill=fg, outline=fg, width=2)
canvas.move(border_download1, 340, 100)

border_uploadbar2 = canvas.create_rectangle(0, 0, 480, 30, fill=bg, outline=fg, width=2)
canvas.move(border_uploadbar2, 340, 150)

border_uploadbar2 = canvas.create_rectangle(0, 0, 168, 30, fill=fg, outline=fg, width=2)
canvas.move(border_uploadbar2, 340, 150)


border_upload1 = canvas.create_rectangle(0, 0, 480, 30, fill=bg, outline=danger_text, width=2)
canvas.move(border_upload1, 340, 250)

border_upload1 = canvas.create_rectangle(0, 0, 400, 30, fill=danger_text, outline=danger_text, width=2)
canvas.move(border_upload1, 340, 250)

border_upload2 = canvas.create_rectangle(0, 0, 480, 30, fill=bg, outline=danger_text, width=2)
canvas.move(border_upload2, 340, 300)

border_upload2 = canvas.create_rectangle(0, 0, 128, 30, fill=danger_text, outline=danger_text, width=2)
canvas.move(border_upload2, 340, 300)

border_countuser = canvas.create_rectangle(0, 0, 370, 50, fill=bg, outline=fg, width=2)
canvas.move(border_countuser, 850, 600)

border_bgcountuser = canvas.create_rectangle(0, 0, 370, 30, fill=fg, outline=fg, width=2)
canvas.move(border_bgcountuser, 850, 650)

# Text display function
def textdisplay(x, y, text='Default text', font=title, textvariable=None, color=fg, bg_text=bg):
    if textvariable != None:
        label = Label(GUI, textvariable=textvariable, font=font, bg=bg_text, fg=fg, justify=LEFT)
    else:
        label = Label(GUI, text=text, font=font, bg=bg_text, fg=color, justify=LEFT)

    label.place(x=x, y=y)


# Status
textdisplay(20, 0, text='/ I N F O /', font=(font_pixel, 10), color=fg)
textdisplay(20,20,text='WiFi name : wifi free no hacking!', font=(font_default, 12), color=status_text)
textdisplay(20, 40, text='IP address : 141.238.138.254', font=(font_default, 12), color=status_text)
textdisplay(20, 60, text='Password : ', font=(font_default, 12), color=status_text)
textdisplay(20, 80, text='Channel : 2.4/5.0', font=(font_default, 12), color=status_text)

# Commands
textdisplay(20, 130, text='/ C O M M A N D S /', font=(font_pixel, 10), color=fg)
textdisplay(20, 150, text='CMD + F : Toggle full screen', font=(font_default, 12), color=status_text)
textdisplay(20, 170, text='CMD + X : Exit program', font=(font_default, 12), color=status_text)
textdisplay(20, 190, text='CMD + D : Hide program', font=(font_default, 12), color=status_text)
textdisplay(20, 210, text='CMD + shift + R : Refresh signal', font=(font_default, 12), color=status_text)
textdisplay(20, 230, text='CMD + Q : Kill program', font=(font_default, 12), color=status_text)
textdisplay(20, 230, text='!stop : Stop program', font=(font_default, 12), color=status_text)
textdisplay(20, 250, text='!clear -l : Clear list', font=(font_default, 12), color=status_text)

textdisplay(20, 390, text='/ T E R M I N A L /', font=(font_pixel, 12), color=fg)
textdisplay(20, 410, text='onze@freewifidevice: $ soft --version', font=(font_default, 12), color=status_text)
textdisplay(20, 430, text='onze@freewifidevice: $ Free wifi no hacking v.1.0.2', font=(font_default, 12), color=status_text)
textdisplay(20, 450, text='onze@freewifidevice: $ soft update lastest', font=(font_default, 12), color=status_text)
textdisplay(20, 470, text='onze@freewifidevice: $ Finding new version...', font=(font_default, 12), color=status_text)
textdisplay(20, 490, text='onze@freewifidevice: $ Downloading version 1.4.2', font=(font_default, 12), color=status_text)
textdisplay(20, 510, text='onze@freewifidevice: $ ########################################### 70.1%', font=(font_default, 12), color=status_text)
textdisplay(20, 530, text='onze@freewifidevice: $ soft installing...', font=(font_default, 12), color=status_text)
textdisplay(20, 550, text='onze@freewifidevice: $ ############################################################ 100.0%', font=(font_default, 12), color=status_text)
textdisplay(20, 570, text='onze@freewifidevice: $ Free wifi no hacking v1.4.2 installed', font=(font_default, 12), color=status_text)
textdisplay(20, 590, text='onze@freewifidevice: $ |', font=(font_default, 12), color=danger_text)


textdisplay(910, 60, text='/ U S E R S /', font=(font_pixel, 12), color=fg)
textdisplay(920, 93, text='#1  uPhone XYZ\n IP: 93.20.174.183 \n MAC: d3-7c-10-bc-eb-7d\n Device: Mobile\n OS: uOS', font=(font_default, 12), color=status_text)
textdisplay(920, 213, text='#2  Meowmi note 7\n IP: 181.7.24.158 \n MAC: 1f-8e-6b-6c-45-94\n Device: Mobile\n OS: Ordroid', font=(font_default, 12), color=status_text)
textdisplay(920, 333, text='#3  Samjung book 21\n IP: 215.84.79.154 \n MAC: a7-e3-c0-09-64-54\n Device: Mobile\n OS: Ordroid', font=(font_default, 12), color=status_text)
textdisplay(920, 453, text='#4  Deal book\n IP: 225.228.70.47 \n MAC: 03-6e-19-44-84-17\n Device: Loptop\n OS: Windy', font=(font_default, 12), color=status_text, bg_text=active_fg)

textdisplay(310, 10, text='/ F I L E  T R A N S F E R /', font=(font_pixel, 12), color=fg)
textdisplay(310, 50, text='Download', font=(font_default, 12), color=status_text)
textdisplay(700, 70, text='Documents/Images', font=(font_default, 12), color=status_text)
textdisplay(310, 200, text='Upload', font=(font_default, 12), color=status_text)
textdisplay(690, 220, text='Desktop/program/spy', font=(font_default, 12), color=status_text)

textdisplay(890, 615, text='Free wifi no hacking!', font=(font_pixel, 16, 'bold'), color=status_text)
textdisplay(980, 655, text='Running...', font=(font_pixel, 12, 'bold'), color=status_text, bg_text=fg)
textdisplay(990, 685, text='v1.4.2', font=(font_pixel, 12, 'bold'), color=status_text)


textdisplay(20, 680, text='SPEED', font=(font_pixel, 28, 'bold'), color=fg)
textdisplay(60, 730, text='433 Mbps', font=(font_pixel, 18), color=danger_text)
textdisplay(220, 680, text='SIGNAL', font=(font_pixel, 28, 'bold'), color=fg)
textdisplay(260, 730, text='VERY GOOD', font=(font_pixel, 18), color=danger_text)
textdisplay(420, 680, text='TYPE', font=(font_pixel, 28, 'bold'), color=fg)
textdisplay(460, 730, text='WPA/WPA2', font=(font_pixel, 18), color=danger_text)



# Clock function
def clockdisplay():
    string = time.strftime('%H:%M:%S')
    lbl.config(text = string)
    lbl.after(1000, clockdisplay)

lbl = Label(GUI, font = (font_pixel, 12),
            background = bg,
            foreground = 'white')

lbl.place(x=1180,y=21)
clockdisplay()

